#!/bin/bash

DOCKER_IMAGE=todo-app:3.0
DOCKER_PORT=3000
HOST_PORT=3000
NAME_PROJECT=app_todo_lite
DOCKER_CONTAINER_NAME=todo-app
DB_USER=todo
DB_PASSWORD=todo
DB_NAME=todo

#Copy ssh key
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFXQwV9pC6hYDJnT6SiWeJJ1xszIFiObkzPU0VHfGjXYHODl0Ke7Dl1oIWHGIOhSlCqgOgyTyuEqJ3kgm4XP7CznQ5+MpmuFlRh0EmJ11qfhh2v18AXKxvYdCYP+OnGKE9HQtkuHNZj7O/ywSSuS55nJbxgx17bVRV4sdjPkNpBOas7i0r9Vw+pHTxRbmk8e5pNZcW3px3fSpUjPh38kcyC7IWQAD/Te8bShBPvKBA70DccgoeNq4bLE564sRRQarj7d0P6ylV3TSkBps7o4/u2kNWEjtn8aA6eldbECsMMJFxsc6JleRCWX3lUnpF7qQOp0LVQ04ujxanDFG2g+XTwPTsU600jRgd2wUFjIRzfJOTqq409ChEoN9wCQpxTEibSMuAl2SUlorDXfUrL687O+rHwkKd53+djmpuBhYak+thw1yQy+CfL/e1VC7udqEXoBcYAxgtdNuomecCdPFZwmc7JMgo7kKLUnImwoKcDgmKIK42SalhJRvrKNaQ4u0= avetis74@avetis74" >> /home/vagrant/.ssh/authorized_keys

# Name our server
echo "192.168.56.3	example.com" >> /etc/hosts

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
# Add the repository to Apt sources:
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Install necessary packages
echo "Установка пакетов для работы приложения"
sudo apt-get install -y git docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin python3-pip
echo "Устновка завершена"

echo "add vagrant in group docker"
sudo adduser vagrant docker

# Install MySQL server in a Non-Interactive mode. Set root username and password for MySQL as desired
echo "Установка mysql-server"
DEBIAN_FRONTEND=noninteractive sudo apt-get install -y mysql-server 
echo "Устновка mysql-server завершена"
# Change bind-address to 0.0.0.0 in /etc/mysql/my.cnf to accept connections from any host
sudo sed -i 's/bind-address.*/bind-address = 0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf

# Restart MySQL server
sudo systemctl restart mysql.service

echo "Создание пользователя и базы данных..."
sudo mysql -uroot -e "CREATE USER '${DB_USER}'@'%' IDENTIFIED BY '${DB_PASSWORD}';"
sudo mysql -uroot -e "CREATE DATABASE ${DB_NAME};"
sudo mysql -uroot -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'%';"
sudo mysql -uroot -e "FLUSH PRIVILEGES;"
echo "Пользователь и база данных созданы!!!!"

# Restart MySQL server
sudo systemctl restart mysql.service

# Clone the desired git repository
echo "Клонируем код проекта"
git clone https://gitlab.com/task_deusops/task_240322/${NAME_PROJECT}.git

echo "change own directory"
sudo chown -R vagrant:vagrant ${NAME_PROJECT}

# Change to the project directory
cd ${NAME_PROJECT}

export MYSQL_USER=todo
export MYSQL_DB=todo
export MYSQL_PASSWORD=todo
export MYSQL_HOST=example.com
export MYSQL_DATABASE=todo
export MYSQL_ROOT_PASSWORD=12345

sudo sed -i 's/MYSQL_USER.*/MYSQL_USER=todo/g' ./.env
sudo sed -i 's/MYSQL_DB.*/MYSQL_DB=todo/g' ./.env
sudo sed -i 's/MYSQL_PASSWORD.*/MYSQL_PASSWORD=todo/g' ./.env
sudo sed -i 's/MYSQL_HOST.*/MYSQL_HOST=example.com/g' ./.env
sudo sed -i 's/MYSQL_DATABASE.*/MYSQL_DATABASE=todo/g' ./.env
sudo sed -i 's/MYSQL_ROOT_PASSWORD.*/MYSQL_ROOT_PASSWORD=12345/g' ./.env

# Build and run the Docker image
echo "Сборка docker image"
docker build -t ${DOCKER_IMAGE} .
echo "Запуск приложения"
docker run -d -p ${HOST_PORT}:${DOCKER_PORT} --env-file .env --name ${DOCKER_CONTAINER_NAME} ${DOCKER_IMAGE}

